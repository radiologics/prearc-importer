/*
 * PrearcImporter: org.nrg.dcm.Restructurer
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.*;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Move;
import org.dcm4che2.data.Tag;
import org.nrg.AbstractRestructurer;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.Utils;
import org.nrg.dicomtools.utilities.DicomUtils;
import org.nrg.framework.status.LoggerStatusReporter;
import org.nrg.util.FileURIOpener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.*;

/**
 * Converts an arbitrary directory structure of DICOM files
 * into a structure we like.
 *
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public final class Restructurer extends AbstractRestructurer {
    private final static String                   MULTIPLE_FILES       = "(selected files)";
    private final static Set<DicomAttributeIndex> patientSelectionKeys = Collections.singleton(Attributes.PatientID);
    private final static String                   defaultSessionName   = "session";
    private final static Set<DicomAttributeIndex> studyLabelKeys       = Collections.singleton((DicomAttributeIndex) new FixedDicomAttributeIndex(Tag.StudyID));
    private final static Set<DicomAttributeIndex> seriesSelectionKeys  = Collections.singleton(Attributes.SeriesInstanceUID);
    private final static Set<DicomAttributeIndex> allKeys              = Collections.unmodifiableSet(new LinkedHashSet<DicomAttributeIndex>() {{
        add(Attributes.StudyInstanceUID);
        addAll(patientSelectionKeys);
        addAll(studyLabelKeys);
        addAll(seriesSelectionKeys);
        add(Attributes.Modality);
        add(Attributes.SeriesNumber);
    }});

    private static final boolean decompressIsSupported = Decompress.isSupported();

    private static final Logger logger = LoggerFactory.getLogger(Restructurer.class);

    private final Collection<File> infiles;
    private final File             outdir;
    private final Project          project;
    private final Set<File> studies = Sets.newLinkedHashSet();

    public Restructurer(final Collection<File> infiles, final File outdir) throws IOException {
        super(logger);

        //noinspection ResultOfMethodCallIgnored
        outdir.mkdirs();
        if (!outdir.isDirectory()) {
            throw new IOException(outdir + " is not a directory");
        }

        this.infiles = ImmutableSet.copyOf(infiles);
        this.outdir = outdir;
        this.project = new Project();
        project.setBaseDir(outdir);
    }

    public Restructurer(final File[] infiles, final File outdir) throws IOException {
        this(Arrays.asList(infiles), outdir);
    }

    public Restructurer(final File infile, final File outdir) throws IOException {
        this(Collections.singleton(infile), outdir);
    }

    @SuppressWarnings({"ResultOfMethodCallIgnored", "ConstantConditions"})
    public void run() {
        if (infiles.isEmpty()) {
            return;
        }

        final Object masterObj;
        if (1 == infiles.size()) {
            masterObj = infiles.iterator().next();
        } else {
            masterObj = MULTIPLE_FILES;
        }
        publishStatus(masterObj, "looking for DICOM files");

        outdir.mkdirs();

        // Find all DICOM files in the indir and sort them by session
        // (this is almost equivalent to DICOM study, but see comments for SessionDirectoryRecordFactory)
        final DicomMetadataStore dicomMetadataStore;
        try {
            dicomMetadataStore = EnumeratedMetadataStore.createHSQLDBBacked(allKeys, FileURIOpener.getInstance());
            dicomMetadataStore.add(Lists.transform(Lists.newArrayList(infiles), new Function<File, URI>() {
                @Nullable
                @Override
                public URI apply(@Nullable final File input) {
                    if (input == null) {
                        return null;
                    }
                    try {
                        return DicomUtils.getQualifiedUri(input.getCanonicalPath());
                    } catch (URISyntaxException e) {
                        return input.toURI();
                    } catch (IOException e) {
                        logger.warn("An error occurred trying to get the URI of the file " + input.getAbsolutePath(), e);
                        return null;
                    }
                }
            }));
            publishStatus(masterObj, "found " + dicomMetadataStore.getSize() + " DICOM files");
        } catch (IOException | SQLException e) {
            publishFailure(masterObj, e.getMessage());
            return;
        }

        try {
            final Set<String> studyUIDs;
            try {
                studyUIDs = dicomMetadataStore.getUniqueValues(Attributes.StudyInstanceUID);
            } catch (ConversionFailureException | IOException | SQLException e) {
                publishFailure(masterObj, e.getMessage());
                return;
            }

            boolean warnedAboutDecompression = false;
            STUDIES:
            for (final String studyUID : studyUIDs) {
                final Map<DicomAttributeIndex, String> studySpec = Collections.singletonMap(Attributes.StudyInstanceUID, studyUID);

                final Set<DicomAttributeIndex> keys = Sets.newLinkedHashSet(patientSelectionKeys);
                keys.addAll(studyLabelKeys);
                keys.add(Attributes.Modality);

                final SetMultimap<DicomAttributeIndex, String> values;
                try {
                    final Map<DicomAttributeIndex, ConversionFailureException> failures = Maps.newHashMap();
                    values = dicomMetadataStore.getUniqueValuesGiven(studySpec, keys, failures);
                    if (!failures.isEmpty()) {
                        publishFailure(masterObj, "Skipping study " + studyUID + ": " + failures.values());
                        continue;
                    }
                } catch (IOException e) {
                    publishFailure(masterObj, "An error occurred reading a file or data stream. Skipping study " + studyUID + ": " + e.getMessage());
                    continue;
                } catch (SQLException e) {
                    publishFailure(masterObj, "An error occurred interacting with the metadata store. Skipping study " + studyUID + ": " + e.getMessage());
                    continue;
                }

                final String label = buildLabel(values, patientSelectionKeys, defaultSessionName);
                publishStatus(masterObj, "found DICOM study " + label + " (" + studyUID + ")");

                final File studyDir = Utils.getUnique(outdir, label);
                studies.add(studyDir);
                final File scansDir = new File(studyDir, SCANS_DIR);
                scansDir.mkdirs();
                if (!studyDir.isDirectory()) {
                    publishFailure(studyDir, "unable to create image directory " + scansDir.getPath());
                    continue;
                }

                final Set<String> seriesUIDs;
                try {
                    final Map<DicomAttributeIndex, ConversionFailureException> failures   = new HashMap<>();
                    final SetMultimap<DicomAttributeIndex, String>             seriesUIDm = dicomMetadataStore.getUniqueValuesGiven(studySpec, seriesSelectionKeys, failures);
                    if (!failures.isEmpty()) {
                        publishFailure(studyDir, "unable to determine series selection keys for study " + label);
                        continue;
                    }

                    seriesUIDs = seriesUIDm.get(Attributes.SeriesInstanceUID);
                    if (null == seriesUIDs) {
                        publishFailure(studyDir, "unable to determine Series Instance UIDs for study " + label);
                        continue;
                    }
                } catch (IOException e) {
                    publishFailure(masterObj, "An error occurred reading a file or data stream. Skipping study " + studyUID + ": " + e.getMessage());
                    continue;
                } catch (SQLException e) {
                    publishFailure(masterObj, "An error occurred interacting with the metadata store. Skipping study " + studyUID + ": " + e.getMessage());
                    continue;
                }

                boolean needsDecompression = false;
                for (final String seriesUID : seriesUIDs) {
                    final Map<DicomAttributeIndex, String> seriesSpec = Maps.newHashMap(studySpec);
                    seriesSpec.put(Attributes.SeriesInstanceUID, seriesUID);
                    final Set<DicomAttributeIndex> seriesAttrs = ImmutableSet.of(Attributes.SeriesNumber, Attributes.TransferSyntaxUID);

                    final Set<URI> files;
                    final File     scanDir;
                    try {
                        final Map<DicomAttributeIndex, ConversionFailureException> failures = Maps.newHashMap();
                        files = dicomMetadataStore.getResourcesForValues(seriesSpec, failures);
                        if (failures.isEmpty()) {
                            final SetMultimap<DicomAttributeIndex, String> metadata = dicomMetadataStore.getUniqueValuesGiven(seriesSpec, seriesAttrs, failures);
                            final Set<String>                              seriesn  = Sets.filter(metadata.get(Attributes.SeriesNumber), Predicates.notNull());
                            if (seriesn.isEmpty()) {
                                scanDir = scansDir;
                            } else {
                                final Iterator<String> sni = seriesn.iterator();
                                if (sni.hasNext()) {
                                    scanDir = new File(scansDir, sni.next());
                                } else {
                                    scanDir = scansDir;
                                }
                            }
                            for (final String tsuid : metadata.get(Attributes.TransferSyntaxUID)) {
                                if (Decompress.needsDecompress(tsuid)) {
                                    needsDecompression = true;
                                    break;
                                }
                            }
                        } else {
                            publishFailure(masterObj, "Skipping series " + seriesUID + " of session " + label + ": " + failures.values());
                            continue STUDIES;
                        }
                    } catch (IOException e) {
                        publishFailure(masterObj, "An error occurred reading a file or data stream. Skipping series " + seriesUID + " of session " + label + ": " + e.getMessage());
                        continue STUDIES;
                    } catch (SQLException e) {
                        publishFailure(masterObj, "An error occurred interacting with the metadata store. Skipping series " + seriesUID + " of session " + label + ": " + e.getMessage());
                        continue STUDIES;
                    }

                    final File scanDicomDataDir = new File(scanDir, "DICOM");

                    for (final URI infileUri : files) {
                        File infile = new File(infileUri);
                        if (needsDecompression) {
                            if (decompressIsSupported) {
                                try {
                                    infile = Decompress.dicomObject2File(Decompress.decompress_image(infile), infile);
                                } catch (Throwable e) {
                                    publishFailure(masterObj, "Decompression error :" + e + ". Storing in original format.");
                                }
                            } else if (!warnedAboutDecompression) {
                                publishWarning(masterObj, "Files in " + label + " need decompression but support is not enabled."
                                                          + " Storing in original format.");
                                warnedAboutDecompression = true;
                            }
                        }

                        // Construct a unique name for the file in the DICOM data directory for this scan.
                        final File outfile  = Utils.getUnique(scanDicomDataDir, infile.getName());
                        final Move moveTask = new Move();
                        moveTask.setProject(project);
                        moveTask.setFile(infile);
                        moveTask.setTofile(outfile);
                        try {
                            moveTask.execute();
                        } catch (BuildException e) {
                            publishWarning(studyDir, e.getMessage());
                        }
                        if (!outfile.exists()) {
                            publishFailure(studyDir, "unable to move " + infile + " to " + outfile);
                        }
                    }
                    try {
                        dicomMetadataStore.remove(Sets.filter(files, new Predicate<URI>() {
                            public boolean apply(final URI uri) {
                                return !new File(uri).exists();
                            }
                        }));
                    } catch (SQLException e) {
                        logger.warn("unable to remove some moved files from metadata store", e);
                    }
                }
            }

            // Remove any remaining empty directory trees.
            pruneDirectoryTree(infiles);

            publishSuccess(masterObj, "done finding DICOM");
        } finally {
            try {
                dicomMetadataStore.close();
            } catch (IOException e) {
                logger.error("metadata database close failed", e);
            }
        }
    }


    private String buildLabel(final SetMultimap<DicomAttributeIndex, String> values,
                              final Collection<DicomAttributeIndex> keys, final String defaultLabel) {
        for (final DicomAttributeIndex key : keys) {
            final Set<String> v = values.get(key);
            if (null != v) {
                final Iterator<String> i = v.iterator();
                if (i.hasNext()) {
                    final String s = i.next();
                    if (null != s) {
                        return s.replaceAll("[\\W]", "_");
                    }
                }
            }
        }
        return defaultLabel;
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.Restructurer#getSessions()
     */
    public Collection<File> getSessions() {
        return studies;
    }


    /**
     * Restructure the specified data.
     *
     * @param args First argument is the pathname of the destination directory. Successive arguments are pathnames to
     *             be restructured.
     *
     * @throws IOException When an error occurs reading or writing data.
     */
    public static void main(final String[] args) throws IOException {
        final File dest = new File(args[0]);
        for (int i = 1; i < args.length; i++) {
            final Restructurer r = new Restructurer(new File(args[i]), dest);
            r.addStatusListener(new LoggerStatusReporter(Restructurer.class));
            r.run();
            System.out.println("Sessions found in " + args[i] + ": " + r.studies);
        }
    }
}
